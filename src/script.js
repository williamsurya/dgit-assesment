(function () {
    fetch('http://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(json => {
            var table = document.getElementById("table");

            var index = 1;
            json.slice().reverse().forEach(function (data) {
                var row = table.insertRow(index);

                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);

                cell1.innerHTML = data.id;
                cell2.innerHTML = data.title;

                //HIGHLIGHT RERUM
                var body = data.body;
                var found = body.search("rerum");
                if (found > -1) {
                    var before = body.substring(0, found);
                    var after = body.substring(found + 5);

                    body = before + "<font class='highlight'>rerum</font>" + after;
                }
                cell3.innerHTML = body;

                row.addEventListener("click", clickPost);
                row.id = data.id;
                row.title = JSON.stringify(data);
            });
        }
        );
})();

function searchChange() {
    var input, filter, table, tr, i, body, title, txtBody, txtTitle;
    input = document.getElementById("searchbar");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        title = tr[i].getElementsByTagName("td")[1];
        body = tr[i].getElementsByTagName("td")[2];
        if (title && body) {
            txtTitle = title.textContent;
            txtBody = body.textContent;
            if (txtTitle.toUpperCase().indexOf(filter) > -1 || txtBody.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function clickPost(evt) {
    var data = JSON.parse(evt.currentTarget.title);
    fetch('http://jsonplaceholder.typicode.com/posts/' + evt.currentTarget.id + '/comments')
        .then(response => response.json())
        .then(json => {
            console.log(json);
            var title = document.getElementById("modalTitle");
            var body = document.getElementById("modalBody");

            title.innerHTML = data.title;
            body.innerHTML = data.body;

            var comment = "";
            json.forEach(function (data) {
                comment += ' <div class="comment">' +
                    '<h5>' + data.name + '</h5>' +
                    '<p class="email">' + data.email + '</p>' +
                    '<p>' + data.body + '</p>' +
                    '</div > ';
            });

            var container_comment = document.getElementById("container-comment");
            container_comment.innerHTML = comment;

            var btnModal = document.getElementById("btnModal");
            btnModal.click();
        }
        );
}
